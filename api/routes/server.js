const express = require("express");
const bodyParser = require("body-parser");
var router = express.Router();

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));

router.get("/api/hello", function(req, res, next) {
  res.send("Hello From Express");
});

router.post("/api/info", (req, res, next) => {
  const topText = req.body.topText;
  const bottomText = req.body.bottomText;
  const image = req.body.setImage;
  res.contentType("image/jpeg");
  res.send(image);
});
module.exports = router;
