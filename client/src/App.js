import React, { Component } from "react";
import "./App.css";

class App extends Component {
  constructor(props) {
    super(props);
    this.handleGenerateBtn = this.handleGenerateBtn.bind(this);
    this.state = {
      title: "Meme Generator",
      topText: "",
      bottomText: "",
      setImage: null,
      response: "",
      responseToPost: ""
    };
  }

  /*componentDidMount() {
    this.callApi()
      .then(res => this.setState({ response: res.express }))
      .catch(err => console.log(err));
  }

  callApi = async () => {
    const response = await fetch("/api/");
    const body = await response.json();
    if (response.status !== 200) throw Error(body.message);
    return body;
  };

  submitHandler = async e => {
    e.preventDefault();
    const response = await fetch("/api/info", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        topText: this.state.topText,
        bottomText: this.state.bottomText,
        setImage: this.state.setImage
      })
    });
    const body = await response.text();
    console.log(body);

    this.setState({ responseToPost: body });
  };*/

  onImageChange = event => {
    if (event.target.files && event.target.files[0]) {
      let reader = new FileReader();
      reader.onload = e => {
        this.setState({
          setImage: e.target.result,
          width: e.target.result.width,
          height: e.target.result.height
        });
      };
      reader.readAsDataURL(event.target.files[0]);
    }
  };

  generateMeme(ctx, canvas, image) {
    ctx.textBaseline = "top";
    ctx.fillText(this.state.topText, canvas.width / 2, 0, canvas.width);
    ctx.strokeText(this.state.topText, canvas.width / 2, 0, canvas.width);
    ctx.textBaseline = "bottom";
    ctx.fillText(
      this.state.bottomText,
      canvas.width / 2,
      canvas.height,
      canvas.width
    );
    ctx.strokeText(
      this.state.bottomText,
      canvas.width / 2,
      canvas.height,
      canvas.width
    );
  }

  handleGenerateBtn = evt => {
    const image = new Image();
    image.src = this.state.setImage;
    let canvas = this.refs.canvas;
    let ctx = canvas.getContext("2d");

    canvas.width = image.width;
    canvas.height = image.height;
    let fontSize = canvas.width / 15;

    ctx.font = fontSize + "px Impact";
    ctx.fillStyle = "white";
    ctx.lineWidth = fontSize / 15;
    ctx.textAlign = "center";
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.drawImage(image, 0, 0);
    this.generateMeme(ctx, canvas, image);
  };

  setText = ({ target }) => {
    this.setState({ [target.name]: target.value });
  };

  render() {
    return (
      <div className="container">
        <header>
          <h1 className="text-center">Meme Generator</h1>
        </header>
        <p>
          <input
            placeholder="Top Text"
            name="topText"
            value={this.state.topText}
            onChange={this.setText}
          />
        </p>
        <p>
          <input
            placeholder="Bottom Text"
            name="bottomText"
            value={this.state.bottomText}
            onChange={this.setText}
          ></input>
        </p>
        <p>
          <input type="file" accept="image" onChange={this.onImageChange} />
        </p>
        <p>
          <button onClick={this.handleGenerateBtn}>Generate Meme</button>
        </p>
        <canvas ref="canvas" style={{ width: 500 }}></canvas>
        {/* <p className="App-intro">{this.state.responseToPost}</p> */}
      </div>
    );
  }
}
export default App;
